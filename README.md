Freifunk Kassel
----------------

Konfiguration für [Gluon](https://github.com/freifunk-gluon/) mit den Daten für das freifunk-Netz in Kassel.
Wir arbeiten mit Gluon 2016.1.

## Building
Um den gluon target `ar71xx-generic` zu bauen führe einfach `make GLUON_TARGET=ar71xx-generic` aus.
